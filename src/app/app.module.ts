import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { HTTP } from '@ionic-native/http/ngx';
import { IonicModule, IonicRouteStrategy, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core'
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SharedModule } from '@shared/shared.module';
import { Camera } from '@ionic-native/camera/ngx';
import { TabsModule } from 'ngx-tabset';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { AgmCoreModule } from '@agm/core';
import { environment } from '@env';
import { ApiWrapper } from '@shared/services/api/api.wrapper';
import { ApiWeb } from '@shared/services/api/api.web';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ApiNative } from '@shared/services/api/api.native';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function HttpWebNativeLoader(
  httpWeb: HttpClient,
  httpNative: HTTP,
  platform: Platform
) {
  if ((window as any).cordova) {
    return new ApiNative(httpNative, platform);
  } else {
    return new ApiWeb(httpWeb);
  }
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    IonicModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    TabsModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: environment.GOOGLE_MAPS_API_KEY
    })
  ],
  providers: [
    PhotoViewer,
    StatusBar,
    SplashScreen,
    Camera,
    Geolocation,
    HTTP,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: ApiWrapper,
      useFactory: HttpWebNativeLoader,
      deps: [HttpClient, HTTP, Platform]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
