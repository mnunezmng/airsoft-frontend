import { Routes } from '@angular/router';

export const ProfileRoutes: Routes = [
  {
    path: 'profile',
    loadChildren: () =>
      import('./pages/profile/profile.module').then((m) => m.ProfilePageModule),
    data: { preload: true }
  }
];
