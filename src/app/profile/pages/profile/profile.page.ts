import { Component } from '@angular/core';
import * as moment from 'moment'

@Component({
  selector: 'app-profile',
  templateUrl: 'profile.page.html',
  styleUrls: ['profile.page.scss'],
})
export class ProfilePage {

  public photo = 'https://i.pravatar.cc/300'

  constructor() {}

  public updateAvatar(image: string) {
    this.photo = image
  }
}
