import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SharedModule } from '@shared/shared.module';
import { ProfilePageRoutingModule } from 'src/app/profile/pages/profile/profile-routing.module';
import { ProfilePage } from 'src/app/profile/pages/profile/profile.page';
import { TabsModule } from 'ngx-tabset';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    ProfilePageRoutingModule,
    TabsModule
  ],
  declarations: [ProfilePage]
})
export class ProfilePageModule {}
