import { Routes } from '@angular/router';

export const LoginRoutes: Routes = [
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/login/login.module').then((m) => m.LoginPageModule),
    data: { preload: true }
  }
];
