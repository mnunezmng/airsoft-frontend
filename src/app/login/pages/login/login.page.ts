import { Component, OnInit } from '@angular/core';
import { AuthService } from '@shared/services/auth/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalService } from '@shared/services/modal/modal.service';
import { NavController } from '@ionic/angular';
import { PAGE_ROUTES } from '@shared/utils/page-routes.utils';
import { GeolocationService } from '@shared/services/geolocation/geolocation.service';
import { UserService } from '@shared/services/user/user.service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage implements OnInit {

  public loginForm: FormGroup
  public loginError: string

  constructor(
    private readonly userService: UserService,
    private readonly formBuilder: FormBuilder,
    private readonly modalService: ModalService,
    private readonly navCtrl: NavController,
    private readonly geolocationService: GeolocationService
  ) {}

  ngOnInit() {
    this.initLoginForm()
    this.setCurrentPosition()
  }

  public async doLogin() {
    await this.modalService.showLoading('LOGIN_PAGE.LOGGING', 'LOGIN_PAGE.WAIT_A_MOMENT')
    const { email, password, rememberMe } = this.loginForm.value
    
    this.userService.signin(email, password, rememberMe)
      .subscribe(
        () => {
          this.loginError = null
          this.navCtrl.navigateForward(PAGE_ROUTES.HOME)
          this.modalService.dismiss()
        },
        () => {
          this.loginError = 'LOGIN_PAGE.LOGIN_ERROR'
          this.modalService.dismiss()
        }
      )
  }

  private initLoginForm() {
    this.loginForm = this.formBuilder.group({
      email: ['mnunez@atsistemas.com', [Validators.required, Validators.email]],
      password: ['1234abcd', Validators.required],
      rememberMe: [false]
    })
  }

  private setCurrentPosition() {
    this.geolocationService.setFirstPosition()
  }
}
