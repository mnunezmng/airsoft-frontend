import { NgModule } from '@angular/core'
import { MainMenuComponent } from '@shared/components/main-menu/main-menu.component'
import { CommonModule } from '@angular/common'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { IonicModule } from '@ionic/angular'
import { TranslateModule } from '@ngx-translate/core'
import { RouterModule } from '@angular/router'
import { MainHeaderComponent } from '@shared/components/main-header/main-header.component'
import { ModalContentComponent } from '@shared/components/modal-content/modal-content.component'
import { PhotoComponent } from '@shared/components/photo/photo.component'
import { HeaderMenuComponent } from '@shared/components/header-menu/header-menu.component'
import { HeaderBackComponent } from '@shared/components/header-back/header-back.component'
import { ImageViewerDirective } from '@shared/directives/image-viewer.directive'
import { ShowMoreComponent } from '@shared/components/show-more/show-more.component'

const components = [
  MainMenuComponent,
  MainHeaderComponent,
  HeaderMenuComponent,
  HeaderBackComponent,
  ModalContentComponent,
  PhotoComponent,
  ShowMoreComponent
]

const pipes = []

const directives = [
  ImageViewerDirective
]

const modules = [
  IonicModule,
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  TranslateModule,
  RouterModule
]

@NgModule({
  declarations: [...components, ...pipes, ...directives],
  entryComponents: [...components],
  imports: [...modules],
  exports: [...components, ...pipes, ...directives, ...modules],
})
export class SharedModule {}
