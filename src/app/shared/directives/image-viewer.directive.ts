import { Directive, HostListener } from '@angular/core';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Directive({
  selector: '[imageViewer]'
})
export class ImageViewerDirective {

  constructor(
    private readonly photoViewer: PhotoViewer
  ) {}

  @HostListener('click', ['$event'])
  clickEvent(event) {
    event.preventDefault();
    event.stopPropagation();
    const src = event.srcElement.currentSrc

    if(!window.cordova) {
      window.open(src)
      return
    }

    this.photoViewer.show(src)
  }
}
