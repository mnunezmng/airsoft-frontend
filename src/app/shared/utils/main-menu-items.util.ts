import { MenuItemInterface } from '@shared/interfaces/menu-item.interface';
import { PAGE_ROUTES } from '@shared/utils/page-routes.utils';

export const MAIN_MENU_ITEMS: MenuItemInterface[] = [
  {
    label: 'MAIN_MENU.HOME',
    icon: 'ios-home',
    url: PAGE_ROUTES.HOME
  },
  {
    label: 'MAIN_MENU.PROFILE',
    icon: 'ios-contact',
    url: PAGE_ROUTES.PROFILE
  },
  {
    label: 'MAIN_MENU.FRIENDS',
    icon: 'ios-people',
    url: PAGE_ROUTES.FRIENDS
  }
]
