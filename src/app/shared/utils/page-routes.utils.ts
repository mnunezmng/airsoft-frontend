export const PAGE_ROUTES = {
  LOGIN: 'login',
  HOME: 'home',
  PROFILE: 'profile',
  FRIENDS: 'friends'
}
