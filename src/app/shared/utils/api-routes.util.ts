export const API_ROUTES = {
  AUTH: {
    SIGNIN: '/auth/signin'
  },
  GAMES: {
    GET_GAMES: '/games'
  }
}
