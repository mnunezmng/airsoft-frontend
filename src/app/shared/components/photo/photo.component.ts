import { Component, Input, Output, EventEmitter } from "@angular/core";
import { CameraService } from '@shared/services/camera/camera.service';
import { CAMERA_SOURCE } from '@shared/enums/camera-source.enum';
import { CAMERA_ERRORS } from '@shared/enums/camera-errors.enum';
import { AlertService } from '@shared/services/alert/alert.service';
import { ActionSheetController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'photo',
  templateUrl: 'photo.component.html',
  styleUrls: ['photo.component.scss']
})
export class PhotoComponent {

  @Input()
  photo: string

  @Output()
  photoChange = new EventEmitter<string>()

  @Input()
  size: number = 80

  @Input()
  editable: boolean = false

  constructor(
    private readonly cameraService: CameraService,
    private readonly alertService: AlertService,
    private readonly actionSheetCtrl: ActionSheetController,
    private readonly translateService: TranslateService
  ) {}

  public async editPhoto() {
    if(!this.editable) {
      return
    }

    const action = await this.actionSheetCtrl.create({
      header: this.translateService.instant('CAMERA.SELECT_IMAGE'),
      buttons: [
        {
          text: this.translateService.instant('CAMERA.CAMERA'),
          icon: 'camera',
          handler: () => this.openCamera(CAMERA_SOURCE.CAMERA)
        },
        {
          text: this.translateService.instant('CAMERA.GALLERY'),
          icon: 'image',
          handler: () => this.openCamera(CAMERA_SOURCE.GALLERY)
        }
      ]
    })
    
    await action.present()
  }

  public getSize() {
    return { width: `${this.size}px`, height: `${this.size}px` }
  }

  private openCamera(source: CAMERA_SOURCE) {
    this.cameraService.getPhoto(source, { allowEdit: true })
      .subscribe(
        res => {
          this.photoChange.emit(res)
        },
        error => {
          if (error === CAMERA_ERRORS.NOT_CORDOVA) {
            this.alertService.notCordova()
          }
        }
      )
  }
}
