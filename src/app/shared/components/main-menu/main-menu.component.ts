import { Component, OnInit } from "@angular/core";
import { MAIN_MENU_ITEMS } from '@shared/utils/main-menu-items.util';
import { MenuItemInterface } from '@shared/interfaces/menu-item.interface';
import { NavController, MenuController } from '@ionic/angular';
import { PAGE_ROUTES } from '@shared/utils/page-routes.utils';

@Component({
  selector: 'main-menu',
  templateUrl: 'main-menu.component.html',
  styleUrls: ['main-menu.component.scss']
})
export class MainMenuComponent {
  public sections = MAIN_MENU_ITEMS

  constructor(
    private readonly navCtrl: NavController,
    private readonly menuCtrl: MenuController
  ) {}

  public navigateTo(section: MenuItemInterface) {
    this.navCtrl.navigateRoot(section.url)
    this.menuCtrl.close()
  }
  
  public logout() {
    this.navCtrl.navigateRoot(PAGE_ROUTES.LOGIN)
    this.menuCtrl.close()
  }
}
