import { Component } from "@angular/core";
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'modal-content',
  templateUrl: 'modal-content.component.html',
  styleUrls: ['modal-content.component.scss']
})
export class ModalContentComponent {

  public icon: string
  public title: string
  public body: string
  public header: boolean
  public isLoading: boolean

  constructor(
    private readonly navParams: NavParams,
    private readonly modalCtrl: ModalController
  ) {
    this.icon = this.navParams.data.icon
    this.title = this.navParams.data.title
    this.body = this.navParams.data.body
    this.header = this.navParams.data.header ? this.navParams.data.header : true
    this.isLoading = !!this.navParams.data.isLoading
  }

  public dismiss() {
    this.modalCtrl.dismiss()
  }
}
