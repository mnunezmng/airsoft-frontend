import { Component, Input } from "@angular/core";
import { Platform } from '@ionic/angular';

@Component({
  selector: 'header-back',
  templateUrl: 'header-back.component.html',
  styleUrls: ['header-back.component.scss']
})
export class HeaderBackComponent {
  @Input()
  title: string

  constructor(
    private readonly platform: Platform
  ) {}

  public get text() {
    return this.platform.is('ios') ? 'COMMON.BACK' : ''
  }
}
