import { Component, Input, ViewChild, ElementRef, OnInit, AfterContentInit } from "@angular/core";

@Component({
  selector: 'show-more',
  templateUrl: 'show-more.component.html',
  styleUrls: ['show-more.component.scss']
})
export class ShowMoreComponent {

  @Input()
  public set minHeight(minHeight: number) {
    this.height = minHeight
    this.startHeight = minHeight
  }  

  @ViewChild('content', { static: true })
  public content: ElementRef

  public active = false
  public height = 0
  
  public get buttonText() {
    return this.active ? 'SHOW_MORE.HIDE' : 'SHOW_MORE.SHOW_MORE'
  }

  private startHeight = 0

  public toggleActive() {
    this.active = !this.active

    this.height = this.active
                ? this.content.nativeElement.offsetHeight
                : this.startHeight
  }
}
