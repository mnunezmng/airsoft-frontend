export interface MenuItemInterface {
  label: string
  icon: string
  url: string
}
