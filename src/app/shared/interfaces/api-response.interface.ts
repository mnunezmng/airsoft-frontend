export interface ApiResponseInterface {
  statusCode: number
  result: any
}
