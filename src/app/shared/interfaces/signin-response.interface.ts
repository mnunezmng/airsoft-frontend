export interface SigninResultInterface {
  statusCode: number
  result: {
    token: string
  }
}
