export interface ApiErrorInterface {
  statusCode: number
  error: string
}
