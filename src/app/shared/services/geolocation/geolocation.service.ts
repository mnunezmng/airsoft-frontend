import { Injectable } from "@angular/core";
import { Geolocation, GeolocationOptions } from "@ionic-native/geolocation/ngx"
import { from, Observable, of } from 'rxjs';
import { GeolocationInterface, CoordsInterface } from '@shared/interfaces/geolocation.interface';
import { tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GeolocationService {

  public position: CoordsInterface
  private geoOptions: GeolocationOptions = {
    enableHighAccuracy: true,
    timeout: 25000
  }

  constructor(
    private readonly geolocation: Geolocation
  ) { }

  public getPosition(force = false): Observable<CoordsInterface> {
    if(force || !this.position) {
      return from(this.geolocation.getCurrentPosition(this.geoOptions))
        .pipe(
          tap(res => {
            this.position = {
              latitude: res.coords.latitude,
              longitude: res.coords.longitude
            }
          }),
          map(() => this.position)
        )
    }

    return of(this.position)
  }

  public setFirstPosition() {
    this.geolocation.getCurrentPosition(this.geoOptions)
      .then(res => {
        this.position = {
          latitude: res.coords.latitude,
          longitude: res.coords.longitude
        }
      })
  }

}
