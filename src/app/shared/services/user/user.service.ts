import { Injectable } from "@angular/core";
import { ApiService } from '@shared/services/api/api.service';
import { Observable } from 'rxjs';
import { SigninResultInterface } from '@shared/interfaces/signin-response.interface';
import { API_ROUTES } from '@shared/utils/api-routes.util';
import { AuthService } from '@shared/services/auth/auth.service';
import { tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private readonly apiService: ApiService,
    private readonly authService: AuthService
  ) { }

  public signin(
    email: string,
    password: string,
    rememberMe: boolean
  ): Observable<SigninResultInterface> {
    return this.apiService.post(API_ROUTES.AUTH.SIGNIN, { email ,password }, true)
      .pipe(
        tap(res => this.authService.setToken(res.result.token)),
        map(res => res.result)
      )
  }

}
