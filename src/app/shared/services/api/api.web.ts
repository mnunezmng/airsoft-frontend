import { Injectable } from "@angular/core";
import { ApiWrapper } from '@shared/services/api/api.wrapper';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { ApiResponseInterface } from '@shared/interfaces/api-response.interface';
import { Observable, throwError } from 'rxjs';
import { ApiErrorInterface } from '@shared/interfaces/api-error.interface';

@Injectable()
export class ApiWeb extends ApiWrapper {

  constructor(
    private readonly http: HttpClient
  ) {
    super()
  }

  public get(
    endpoint: string,
    params: object,
    headers: HttpHeaders
  ): Observable<ApiResponseInterface> {
    const options: any = {
      observe: 'response',
      headers,
      params
    };

    return this.http.get(
      endpoint,
      options
    )
      .pipe(
        map((res: any) => res.body),
        catchError(error => throwError(this.transformError(error)))
      )
  }

  public post(
    endpoint: string,
    body: object,
    headers: HttpHeaders
  ): Observable<ApiResponseInterface> {
    const options: any = {
      observe: 'response',
      headers,
    };

    return this.http.post(
      endpoint,
      body,
      options
    )
      .pipe(
        map((res: any) => res.body),
        catchError(error => throwError(this.transformError(error)))
      )
  }

  public delete(
    endpoint: string,
    params: object,
    headers: HttpHeaders
  ) {
    const options: any = {
      observe: 'response',
      headers,
      params
    };

    return this.http.delete(
      endpoint,
      options
    )
      .pipe(
        map((res: any) => res.body),
        catchError(error => throwError(this.transformError(error)))
      )
  }

  public put(
    endpoint: string,
    body: object,
    headers: HttpHeaders
  ): Observable<ApiResponseInterface> {
    const options: any = {
      observe: 'response',
      headers,
    };

    return this.http.put(
      endpoint,
      body,
      options
    )
      .pipe(
        map((res: any) => res.body),
        catchError(error => throwError(this.transformError(error)))
      )
  }

  private transformError(error: HttpErrorResponse): ApiErrorInterface {
    return {
      statusCode: error.status,
      error: error.message
    }
  }
}
