import { Injectable } from "@angular/core";
import { ApiWrapper } from '@shared/services/api/api.wrapper';
import { map, catchError } from 'rxjs/operators';
import { ApiResponseInterface } from '@shared/interfaces/api-response.interface';
import { Observable, throwError, from } from 'rxjs';
import { ApiErrorInterface } from '@shared/interfaces/api-error.interface';
import { HTTP, HTTPResponse } from '@ionic-native/http/ngx';
import { Platform } from '@ionic/angular';

@Injectable()
export class ApiNative extends ApiWrapper {

  constructor(
    private readonly http: HTTP,
    private readonly platform: Platform
  ) {
    super()
    this.platform.ready().then(() => this.init());
  }

  public get(
    endpoint: string,
    params: object,
    headers: any
  ): Observable<ApiResponseInterface> {
    return from(
      this.http.get(
        encodeURI(endpoint),
        params,
        headers
      )
    )
      .pipe(
        map((res: HTTPResponse) => this.transformResponse(res)),
        catchError(error => throwError(this.transformError(error)))
      )
  }

  public post(
    endpoint: string,
    body: object,
    headers: any
  ): Observable<ApiResponseInterface> {
    return from(
      this.http.post(
        encodeURI(endpoint),
        body,
        headers
      )
    )
      .pipe(
        map((res: HTTPResponse) => this.transformResponse(res)),
        catchError(error => throwError(this.transformError(error)))
      )
  }

  public delete(
    endpoint: string,
    params: object,
    headers: any
  ) {
    return from(
      this.http.delete(
        encodeURI(endpoint),
        params,
        headers
      )
    )
      .pipe(
        map((res: HTTPResponse) => this.transformResponse(res)),
        catchError(error => throwError(this.transformError(error)))
      )
  }

  public put(
    endpoint: string,
    body: object,
    headers: any
  ): Observable<ApiResponseInterface> {
    return from(
      this.http.put(
        encodeURI(endpoint),
        body,
        headers
      )
    )
      .pipe(
        map((res: HTTPResponse) => this.transformResponse(res)),
        catchError(error => throwError(this.transformError(error)))
      )
  }

  private transformResponse(response: HTTPResponse): ApiResponseInterface {
    let result: any = null;

    if (response.data) {
      result = JSON.parse(response.data).result;
    }

    return {
      result,
      statusCode: response.status
    };
  }

  private transformError(res): ApiErrorInterface {
    console.log('ERROR', res)
    return {
      error: res.error,
      statusCode: res.staus
    };
  }

  private init() {
    this.http.setDataSerializer('json')
  }
}
