import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiResponseInterface } from '@shared/interfaces/api-response.interface';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export abstract class ApiWrapper {

  public abstract get(
    endpoint: string,
    params: object,
    headers: any
  ): Observable<ApiResponseInterface>

  public abstract post(
    endpoint: string,
    body: object,
    headers: any
  ): Observable<ApiResponseInterface>

  public abstract delete(
    endpoint: string,
    params: object,
    headers: any,
  ): Observable<ApiResponseInterface>

  public abstract put(
    endpoint: string,
    body: object,
    headers: any
  ): Observable<ApiResponseInterface>

}
