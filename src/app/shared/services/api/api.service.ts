import { Injectable } from "@angular/core";
import { ApiWrapper } from '@shared/services/api/api.wrapper';
import { Observable } from 'rxjs';
import { ApiResponseInterface } from '@shared/interfaces/api-response.interface';
import { HttpHeaders } from '@angular/common/http';
import { AuthService } from '@shared/services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  
  constructor(
    private readonly apiWrapper: ApiWrapper,
    private readonly authService: AuthService
  ) { }

  public get(
    endpoint: string,
    params: object,
    omitBearer = false
  ): Observable<ApiResponseInterface> {
    return this.apiWrapper.get(
      this.authService.generateEndpoint(endpoint),
      params,
      this.authService.generateHeaders(omitBearer)
    )
  }

  public post(
    endpoint: string,
    body: object,
    omitBearer = false
  ): Observable<ApiResponseInterface> {
    return this.apiWrapper.post(
      this.authService.generateEndpoint(endpoint),
      body,
      this.authService.generateHeaders(omitBearer)
    )
  }

  public delete(
    endpoint: string,
    params: object,
    omitBearer: boolean
  ): Observable<ApiResponseInterface> {
    return this.apiWrapper.delete(
      this.authService.generateEndpoint(endpoint),
      params,
      this.authService.generateHeaders(omitBearer)
    )
  }
  
  public put(
    endpoint: string,
    body: object,
    omitBearer: boolean
  ): Observable<ApiResponseInterface> {
    return this.apiWrapper.put(
      this.authService.generateEndpoint(endpoint),
      body,
      this.authService.generateHeaders(omitBearer)
    )
  }  
}
