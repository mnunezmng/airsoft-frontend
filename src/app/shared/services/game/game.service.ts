import { Injectable } from "@angular/core";
import { ApiService } from '@shared/services/api/api.service';
import { API_ROUTES } from '@shared/utils/api-routes.util';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(
    private readonly apiService: ApiService
  ) {}

  getGames(geo: any, skip: number, limit: number) {
    return this.apiService.get(
      API_ROUTES.GAMES.GET_GAMES,
      {}
    )
    .pipe(
      map(res => res.result)
    )
  }

}
