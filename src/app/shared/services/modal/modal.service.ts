import { Injectable } from "@angular/core";
import { ModalController, ToastController } from '@ionic/angular';
import { ModalContentComponent } from '@shared/components/modal-content/modal-content.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  
  constructor(
    private readonly modalCtrl: ModalController
  ) {}

  public showLoading(
    title: string,
    body: string
  ) {
    return this.modalCtrl.create({
      component: ModalContentComponent,
      componentProps: {icon: null, title, body, isLoading: true, header: false }
    })
    .then(async (res) => {
      await res.present()
      return res
    })
  }

  public dismiss() {
    this.modalCtrl.dismiss()
  }
}
