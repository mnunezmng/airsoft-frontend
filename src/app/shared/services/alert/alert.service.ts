import { Injectable } from "@angular/core";
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(
    private readonly alertCtrl: AlertController,
    private readonly translateService: TranslateService
  ) {}

  public async notCordova() {
    const alert = await this.alertCtrl.create({
      header: this.translateService.instant('ERRORS.ERROR_OCURRED'),
      message: this.translateService.instant('ERRORS.NOT_CORDOVA'),
      buttons: [this.translateService.instant('COMMON.CLOSE')]
    });

    alert.present()
    return alert
  }

}
