import { Injectable } from "@angular/core";
import { ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(
    private readonly toastCtrl: ToastController,
    private readonly translateService: TranslateService
  ) { }

  public async showRequestError(text: string = 'ERROR_OCURRED') {
    const toast = await this.toastCtrl.create({
      header: this.translateService.instant(`REQUEST_ERRORS.${text.toUpperCase()}`),
      position: 'top',
      color: 'danger',
      duration: 2500
    })

    await toast.present()
  }

  public async showRequestSuccess(text: string = 'COMMON.SUCCESS_REQUEST') {
    const toast = await this.toastCtrl.create({
      header: this.translateService.instant(text.toUpperCase()),
      position: 'bottom',
      color: 'success',
      duration: 2500
    })

    await toast.present()
  }
}
