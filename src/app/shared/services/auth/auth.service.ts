import { Injectable } from "@angular/core";
import { environment } from '@env';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _token: string
  public get token() {
    return this._token
  }

  public generateHeaders(omitBearer: boolean) {
    let headers: any = {}

    if (!omitBearer) {
      headers.Authorization = `Bearer ${this.token}`
    }

    return headers
  }

  public generateEndpoint(endpoint: string): string {
    return `${environment.API_URL}${endpoint}`
  }

  public setToken(token: string) {
    this._token = token
    localStorage.setItem('token', token)
  }

}
