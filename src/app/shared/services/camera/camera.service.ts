import { Injectable } from "@angular/core";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx"
import { CAMERA_SOURCE } from '@shared/enums/camera-source.enum';
import { from, Observable, throwError } from 'rxjs';
import { CAMERA_ERRORS } from "@shared/enums/camera-errors.enum"
import { map } from "rxjs/operators"

@Injectable({
  providedIn: 'root'
})
export class CameraService {
  
  constructor(
    private readonly camera: Camera
  ) {}

  getPhoto(source: CAMERA_SOURCE, options?: CameraOptions): Observable<string> {
    const cameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: source,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: false,
      correctOrientation: true,
      ...options
    }

    if(!window.cordova) {
      return throwError(CAMERA_ERRORS.NOT_CORDOVA)
    }

    return from(this.camera.getPicture(cameraOptions))
      .pipe(map(res => `data:image/jpg;base64,${res}`))
  }
}
