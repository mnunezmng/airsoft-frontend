import { NgModule } from '@angular/core';
import { GamePage } from 'src/app/game/pages/game/game.page';
import { GamePageRoutingModule } from 'src/app/game/pages/game/game-routing.module';
import { SharedModule } from '@shared/shared.module';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [
    GamePageRoutingModule,
    SharedModule,
    AgmCoreModule
  ],
  declarations: [GamePage]
})
export class GamePageModule { }
