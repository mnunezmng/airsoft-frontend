import { Routes } from '@angular/router';

export const GameRoutes: Routes = [
  {
    path: 'game',
    loadChildren: () =>
      import('./pages/game/game.module').then((m) => m.GamePageModule),
    data: { preload: false }
  }
];
