import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeRoutes } from 'src/app/home/routes';
import { SimpleLoadingStrategy } from '@shared/utils/simple-loading-strategy.util';
import { LoginRoutes } from 'src/app/login/routes';
import { ProfileRoutes } from 'src/app/profile/routes';
import { GameRoutes } from 'src/app/game/routes';
import { FriendsRoutes } from 'src/app/friends/routes';

const ROUTES: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  ...LoginRoutes,
  ...HomeRoutes,
  ...ProfileRoutes,
  ...GameRoutes,
  ...FriendsRoutes
];

@NgModule({
  providers: [SimpleLoadingStrategy],
  imports: [
    RouterModule.forRoot(
      ROUTES,
      {
        useHash: true,
        preloadingStrategy: SimpleLoadingStrategy
      }
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
