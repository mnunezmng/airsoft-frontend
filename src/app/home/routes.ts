import { Routes } from '@angular/router';

export const HomeRoutes: Routes = [
  {
    path: 'home',
    loadChildren: () =>
      import('./pages/home/home.module').then((m) => m.HomePageModule),
    data: { preload: true }
  }
];
