import { NgModule } from '@angular/core';
import { GameCardComponent } from 'src/app/home/components/game-card/game-card.component';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [GameCardComponent],
  imports: [IonicModule, CommonModule],
  exports: [GameCardComponent]
})
export class GameCardModule {}
