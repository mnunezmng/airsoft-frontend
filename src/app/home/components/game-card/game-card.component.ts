import { Component, Input } from "@angular/core";
import { Router } from '@angular/router';

@Component({
  selector: 'game-card',
  templateUrl: 'game-card.component.html',
  styleUrls: ['game-card.component.scss']
})
export class GameCardComponent {

  @Input()
  skeleton: boolean

  @Input()
  game

  constructor(
    private readonly router: Router
  ) {}

  public goToGame() {
    console.log('navigate!')
    this.router.navigateByUrl('game')
  }
}
