import { Component, ViewChild, OnInit } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { GameService } from '@shared/services/game/game.service';
import { GeolocationService } from '@shared/services/geolocation/geolocation.service';
import { flatMap } from 'rxjs/operators';
import { ToastService } from '@shared/services/toast/toast.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll

  public games
  public get showSkeleton() {
    return !this.games
  }
  public get showEmpty() {
    return this.games && !this.games.length
  }
  public get showGames() {
    return this.games && this.games.length
  }

  constructor(
    private readonly gameService: GameService,
    private readonly geolocationService: GeolocationService,
    private readonly toastService: ToastService
  ) {}

  ngOnInit() {
    this.getGames()
  }

  public loadData(event) {
    setTimeout(() => {
      this.games = this.games.concat([1, 2, 3])
      event.target.complete()
    }, 1000)
  }

  public toggleScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled
  }

  private getGames() {
    // this.geolocationService.getPosition()
    //   .pipe(
    //     flatMap(res => this.gameService.getGames(res, 2, 0))
    //   )
    //   .subscribe(
    //     games => {
    //       console.log(games)
    //       this.games = games
    //     },
    //     () => {
    //       this.games = []
    //       this.toastService.showRequestError()
    //     }
    //   )
    this.gameService.getGames({}, 2, 0)
      .subscribe(
        games => {
          console.log(games)
          this.games = games
        },
        () => {
          this.games = []
          this.toastService.showRequestError()
        }
      )
  }
}
