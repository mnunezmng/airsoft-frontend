import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';
import { SharedModule } from '@shared/shared.module';
import { GameCardModule } from 'src/app/home/components/game-card/game-card.module';
import { HomePageRoutingModule } from 'src/app/home/pages/home/home-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    HomePageRoutingModule,
    GameCardModule
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
