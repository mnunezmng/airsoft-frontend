import { Routes } from '@angular/router';

export const FriendsRoutes: Routes = [
  {
    path: 'friends',
    loadChildren: () =>
      import('./pages/friends/friends.module').then((m) => m.FriendsPageModule),
    data: { preload: false }
  }
];
