import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { FriendsPage } from 'src/app/friends/pages/friends/friends.page';
import { FriendsPageRoutingModule } from 'src/app/friends/pages/friends/friends-routing.module';

@NgModule({
  imports: [
    FriendsPageRoutingModule,
    SharedModule
  ],
  declarations: [FriendsPage]
})
export class FriendsPageModule { }
